//
//  ViewController.swift
//  dataPicker
//
//  Created by MAC on 11.11.2021.
//

import UIKit

class ViewController: UIViewController {
    let picker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.center = view.center
        picker.datePickerMode = .countDownTimer
        //protocol
//        picker.dataSource = self
//        picker.delegate = self
        self.view.addSubview(picker)
        var oneYearTime = TimeInterval()    
        oneYearTime = 365 * 24 * 60 * 60
        let todayData = Date()
        let oneYearFromToday = todayData.addingTimeInterval(oneYearTime)
        let twoYearFromDate = todayData.addingTimeInterval(2 * oneYearTime)
        picker.maximumDate = oneYearFromToday
        picker.maximumDate = twoYearFromDate
        picker.countDownDuration = 2 * 60
//    }
//
//}
//extension ViewController :UIPickerViewDataSource {
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        10
//    }
//
//
//}
//extension ViewController: UIPickerViewDelegate {
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        let result = "row \(row)"
//        return result
//    }
//}
        picker.addTarget(self, action:#selector(dataPickerChange(paramdataPicker:)), for: .valueChanged)
    }
    @objc func dataPickerChange(paramdataPicker: UIDatePicker) {
        if paramdataPicker.isEqual(self.picker) {
            print("dataChange: = ", paramdataPicker.date)

        }
    }
}
