//
//  ViewController.swift
//  lernUI
//
//  Created by MAC on 10.11.2021.
//

import UIKit

class ViewController: UIViewController {
    let mySwitch = UISwitch()
    let button = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.button.frame = CGRect(x: 100, y: 200, width: 200, height: 100)
        self.button.backgroundColor = UIColor.orange
        self.button.setTitle("Ok", for: .normal)
        self.button.setTitle("on button", for: .highlighted)
        self.view.addSubview(self.button)
        self.mySwitch.frame = CGRect.zero
        self.mySwitch.center = self.view.center
        self.view.addSubview(self.mySwitch)
        self.mySwitch.tintColor = UIColor.green
        self.mySwitch.thumbTintColor = UIColor.red
        self.mySwitch.onTintColor = UIColor.blue
        self.mySwitch.addTarget(self, action: #selector(isOn(target:)), for: .editingChanged)
        
        
    }
    @objc func isOn(target: UISwitch) {
        if target.isOn {
            self.button.isUserInteractionEnabled = false
        }else {
            self.button.isUserInteractionEnabled = true
        }
    }
}

